package com.cosmenp.springbootwork;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String path = System.getProperty("user.home") + "/uploads_nieto_srping/";
        registry.addResourceHandler("/upload/**").
                addResourceLocations("file:" + path); // con esto indicamos el directorio real
    }
}
