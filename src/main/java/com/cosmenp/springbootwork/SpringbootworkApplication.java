package com.cosmenp.springbootwork;

import com.cosmenp.springbootwork.service.impl.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.nio.file.FileAlreadyExistsException;

@SpringBootApplication
public class SpringbootworkApplication implements CommandLineRunner {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    FileService fileService;

    public static void main(String[] args) {
        SpringApplication.run(SpringbootworkApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        String passBCript = passwordEncoder.encode("admin");
//        System.out.println("Contraseña encriptada inicial de admin (admin): "+passBCript);
        try {
            fileService.init();
        } catch (FileAlreadyExistsException ignored) {
        }
    }
}
