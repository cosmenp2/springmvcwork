package com.cosmenp.springbootwork.controller;

import com.cosmenp.springbootwork.model.entity.Article;
import com.cosmenp.springbootwork.service.impl.ArticleService;
import com.cosmenp.springbootwork.service.impl.FileService;
import com.cosmenp.springbootwork.service.impl.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/public/article")
public class PublicArticleController {
    @Autowired
    ArticleService articleService;
    @Autowired
    ProviderService providerService;
    @Autowired
    FileService fileService;

    @GetMapping("/list")
    public String list(Model model) {
        List<Article> articles = articleService.listActive();
        if (articles == null)
            articles = new ArrayList<>();
        model.addAttribute("articles", articles);
        return "/public/article/list";
    }

    @GetMapping(path = "/get/{id}")
    public String get(@PathVariable String id, Model model) {
        String destination = "/error/404";
        Article article = articleService.get(id);
        if (article != null) {
            if (article.getActive() == 1 || article.getStock() > 0) {
                model.addAttribute("article", article);
                destination = "public/article/get";
            }
        }
        return destination;
    }
}
