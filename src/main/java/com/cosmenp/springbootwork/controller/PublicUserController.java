package com.cosmenp.springbootwork.controller;

import com.cosmenp.springbootwork.model.entity.Client;
import com.cosmenp.springbootwork.model.entity.User;
import com.cosmenp.springbootwork.service.impl.ClientService;
import com.cosmenp.springbootwork.service.impl.FileService;
import com.cosmenp.springbootwork.service.impl.RoleService;
import com.cosmenp.springbootwork.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;

@Controller
@RequestMapping(path = "/public/user")
public class PublicUserController {
    @Autowired
    UserService userService;
    @Autowired
    FileService fileService;
    @Autowired
    RoleService roleService;
    @Autowired
    ClientService clientService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping(path = "/register")
    public String add(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("client", new Client());
        return "public/user/register";
    }

    @PostMapping(path = "/register")
    public String add(@Valid User user, BindingResult resultUser, @Valid Client client, BindingResult resultClient,
                      Model model, @RequestParam("file") MultipartFile photo, RedirectAttributes flash) {
        String destination;
        boolean error = false;
        if (user.getUsername() != null) {
            User temp = userService.getByUsername(user.getUsername());
            if (temp != null) {
                resultUser.addError(new FieldError("user", "username",
                        "El nombre de usuario está ocupado. Pruebe con otro."));
            }
        }
        if (user.getEmail() != null) {
            User temp = userService.getByEmail(user.getEmail());
            if (temp != null) {
                resultUser.addError(new FieldError("user", "email",
                        "Ya existe un usuario con este correo electrónico"));
            }
        }
        if (client.getNif() != null) {
            Client temp = clientService.getByNif(client.getNif());
            if (temp != null) {
                resultClient.addError(new FieldError("client", "client.nif",
                        "Ya existe un cliente con este nif"));
            }
        }

        if (resultUser.hasErrors() || resultClient.hasErrors()) {
            error = true;
        } else {
            client.setIdClient(clientService.newCode(client.getPostalCode()));
            if (!photo.isEmpty()) {
                try {
                    fileService.copy(photo, "client_" + client.getIdClient());
                    client.setPhoto("client_" + client.getIdClient());
                } catch (IOException ignored) {
                    error = true;
                    flash.addFlashAttribute("errors", "No se ha podido subir la foto");
                }
            }
            if (client.getSurname2().isEmpty()) {
                client.setSurname2(null);
            }
            if (client.getBusinessName().isEmpty()) {
                client.setBusinessName(null);
            }
            user.setClient(client);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setRole(roleService.findByRoleName("USER"));
            user.setActive(1);
            try {
                userService.save(user);
                flash.addFlashAttribute("success", "Se ha registrado correctamente. " +
                        "Inicie sesión para comenzar.");
            } catch (Exception e) {
                error = true;
                flash.addFlashAttribute("errors", "Ha habido un fallo al registrarse, " +
                        "intentelo de nuevo más tarde");
            }
        }
        if (error) {
            destination = "public/user/register";
        } else {
            destination = "redirect:/public/user/login";
        }
        return destination;
    }

    @GetMapping(path = "/login")
    public String login(@RequestParam(value = "error", required = false) String error, Model model, Principal principal, RedirectAttributes flash) {
        if (principal != null) {
            flash.addFlashAttribute("info", "Tiene una sesion iniciada");
            return "redirect:/";
        }
        if (error != null) {
            model.addAttribute("errors", "Nombre de usuario o contraseña incorrecta");
        }
        return "public/user/login";
    }
}
