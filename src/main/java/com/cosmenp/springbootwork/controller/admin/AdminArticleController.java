package com.cosmenp.springbootwork.controller.admin;

import com.cosmenp.springbootwork.model.entity.Article;
import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.entity.OrderLine;
import com.cosmenp.springbootwork.model.entity.Provider;
import com.cosmenp.springbootwork.service.impl.ArticleService;
import com.cosmenp.springbootwork.service.impl.FileService;
import com.cosmenp.springbootwork.service.impl.OrderService;
import com.cosmenp.springbootwork.service.impl.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/admin/article")
public class AdminArticleController {
    @Autowired
    ArticleService articleService;
    @Autowired
    ProviderService providerService;
    @Autowired
    OrderService orderService;
    @Autowired
    FileService fileService;

    Article oldArticle;

    @GetMapping(path = "/add")
    public String add(Model model) {
        model.addAttribute("article", new Article());
        model.addAttribute("providers", providerService.listActive());
        return "admin/article/add";
    }

    @PostMapping(path = "/add")
    public String add(@Valid Article article, BindingResult result, Model model,
                      @RequestParam("file") MultipartFile photo, RedirectAttributes flash) {
        String destination;
        boolean error = false;
        Provider provider = providerService.get(article.getProvider().getIdProvider());
        if (provider != null) {
            if (provider.getActive() == 1)
                article.setProvider(provider);
            else {
                result.addError(new FieldError("article", "provider.idProvider",
                        "Proveedor no válido (no está activo)"));
            }
        } else {
            result.addError(new FieldError("article", "provider.idProvider",
                    "Este campo es obligatorio"));
        }
        if (result.hasErrors()) {
            error = true;
        } else {
            article.setIdArticle(articleService.newCode(article.getProvider().getIdProvider()));
            if (!photo.isEmpty()) {
                try {
                    fileService.copy(photo, "article_" + article.getIdArticle());
                    article.setPhoto("article_" + article.getIdArticle());
                } catch (IOException ignored) {
                    error = true;
                    flash.addFlashAttribute("errors", "No se ha podido subir la foto");
                }
            }
            if (!error) {
                article.setActive(1);
                try {
                    articleService.save(article);
                    flash.addFlashAttribute("success", "El artículo se ha guardado correctamente");
                } catch (Exception e) {
                    error = true;
                    flash.addFlashAttribute("errors", "El artículo no se ha podido guardar");
                }
            }
        }
        if (error) {
            destination = "admin/article/add";
            model.addAttribute("providers", providerService.listActive());
        } else {
            destination = "redirect:/admin/article/add";
        }
        return destination;
    }

    @GetMapping(path = "/edit/{id}")
    public String edit(@PathVariable String id, Model model) {
        oldArticle = articleService.get(id);
        if (oldArticle != null) {
            model.addAttribute("article", oldArticle);
            return "admin/article/edit";
        } else {
            return "error/404";
        }
    }

    @PostMapping(path = "/edit")
    public String edit(@Valid Article article, BindingResult result, Model model,
                       @RequestParam("file") MultipartFile photo, RedirectAttributes flash) {
        String destination;
        article.setIdArticle(oldArticle.getIdArticle());
        article.setId(oldArticle.getId());
        List<ObjectError> errors = result.getAllErrors();
        boolean error = false;
        if (errors.size() > 1) {
            error = true;
        } else {
            if (!photo.isEmpty()) {
                try {
                    fileService.copy(photo, "article_" + article.getIdArticle());
                    article.setPhoto("article_" + article.getIdArticle());
                } catch (IOException ignored) {
                    error = true;
                    flash.addFlashAttribute("errors", "No se ha podido subir la foto");
                }
            } else {
                article.setPhoto(oldArticle.getPhoto());
            }
            if (!error) {
                article.setProvider(oldArticle.getProvider());
                article.setActive(oldArticle.getActive());
                article.setStock(oldArticle.getStock());
                try {
                    articleService.update(article);
                    flash.addFlashAttribute("success", "El artículo se ha actualizado correctamente");
                    oldArticle = null;
                } catch (Exception e) {
                    error = true;
                    flash.addFlashAttribute("errors", "El artículo no se ha podido actualizar");
                }
            }
        }
        if (error) {
            destination = "/admin/article/edit";
        } else {
            destination = "redirect:/admin/article/get/" + article.getIdArticle();
        }
        return destination;
    }

    @GetMapping("/list")
    public String list(Model model) {
        List<Article> articles = articleService.list();
        if (articles == null)
            articles = new ArrayList<>();
        model.addAttribute("articles", articles);
        return "/admin/article/list";
    }

    @GetMapping(path = "/get/{id}")
    public String get(@PathVariable String id, Model model) {
        Article article = articleService.get(id);
        String destination = "/error/404";
        if (article != null) {
            List<Order> orders = orderService.listByArticle(id);
            if(orders == null){
                orders = new ArrayList<>();
            }
            double totalSold = 0.0;
            int unitsSold = 0;
            for (Order order : orders) {
                for (OrderLine line : order.getOrderLines()) {
                    if (line.getArticle().getIdArticle().equals(id)) {
                        totalSold = Math.rint((line.getTotalLine() + totalSold) * 100) / 100;
                        unitsSold += line.getUnits();
                    }
                }
            }
            model.addAttribute("article", article);
            model.addAttribute("orders", orders);
            model.addAttribute("totalSold", totalSold);
            model.addAttribute("unitsSold", unitsSold);
            destination = "admin/article/get";
        }
        return destination;
    }

    @PostMapping(path = "/get")
    public String get(Integer newStock, String idArticle, RedirectAttributes flash) {
        Article article = articleService.get(idArticle);
        if (article != null) {
            try {
                if (article.getActive() == 0) {
                    if (newStock < article.getStock()) {
                        article.setStock(newStock);
                        articleService.update(article);
                        flash.addFlashAttribute("success", "El stock del artículo se ha actualizado correctamente");
                    } else {
                        flash.addFlashAttribute("errors",
                                "No se puede aumentar el stock del artículo si está desactivado");
                    }
                } else {
                    article.setStock(newStock);
                    articleService.update(article);
                    flash.addFlashAttribute("success", "El stock del artículo se ha actualizado correctamente");
                }
            } catch (Exception e) {
                flash.addFlashAttribute("errors", "El stock del artículo no se ha podido actualizar correctamente");
            }
        } else {
            flash.addFlashAttribute("errors", "¡El artículo no existe!");
        }

        return "redirect:/admin/article/get/" + idArticle;
    }

    @GetMapping(path = "/disable/{id}")
    public String disable(@PathVariable String id, RedirectAttributes flash) {
        disableArticle(id, flash);
        return "redirect:/admin/article/list";
    }

    @GetMapping(path = "/get/disable/{id}")
    public String disableOnGet(@PathVariable String id, RedirectAttributes flash) {
        disableArticle(id, flash);
        return "redirect:/admin/article/get/" + id;
    }

    private void disableArticle(@PathVariable String id, RedirectAttributes flash) {
        Article article = articleService.get(id);
        if (article != null) {
            articleService.disable(id);
            flash.addFlashAttribute("success", "El artículo " +
                    article.getIdArticle() + " se ha desactivado correctamente");
        } else {
            flash.addFlashAttribute("errors", "El artículo " + id + " no se ha podido desactivar correctamente");
        }
    }

    @GetMapping(path = "/enable/{id}")
    public String enable(@PathVariable String id, RedirectAttributes flash) {
        enableArticle(id, flash);
        return "redirect:/admin/article/list";
    }

    @GetMapping(path = "/get/enable/{id}")
    public String enableOnGet(@PathVariable String id, RedirectAttributes flash) {
        enableArticle(id, flash);
        return "redirect:/admin/article/get/" + id;
    }

    private void enableArticle(@PathVariable String id, RedirectAttributes flash) {
        Article article = articleService.get(id);
        if (article != null) {
            if (article.getProvider().getActive() == 1) {
                articleService.enable(id);
                flash.addFlashAttribute("success", "El artículo " +
                        article.getIdArticle() + " se ha activado correctamente");
            } else
                flash.addFlashAttribute("errors", "El proveedor no está activo");
        } else {
            flash.addFlashAttribute("errors", "El artículo " + id + " no se ha podido activar correctamente");
        }
    }
}
