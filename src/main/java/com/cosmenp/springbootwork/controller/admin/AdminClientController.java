package com.cosmenp.springbootwork.controller.admin;

import com.cosmenp.springbootwork.model.entity.Client;
import com.cosmenp.springbootwork.service.impl.ClientService;
import com.cosmenp.springbootwork.service.impl.FileService;
import com.cosmenp.springbootwork.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/admin/client")
public class AdminClientController {
    @Autowired
    ClientService clientService;
    @Autowired
    UserService userService;
    @Autowired
    FileService fileService;

    Client oldClient;

    @GetMapping(path = "/add")
    public String add(Model model) {
        model.addAttribute("client", new Client());
        return "admin/client/add";
    }

    @PostMapping(path = "/add")
    public String add(@Valid Client client, BindingResult result, Model model,
                      @RequestParam("file") MultipartFile photo, RedirectAttributes flash) {
        String destination;
        boolean error = false;
        if (client.getNif() != null) {
            Client temp = clientService.getByNif(client.getNif());
            if (temp != null) {
                result.addError(new FieldError("client", "nif",
                        "Ya existe un cliente con este nif"));
            }
        }
        if (result.hasErrors()) {
            error = true;
        } else {
            client.setIdClient(clientService.newCode(client.getPostalCode()));
            if (!photo.isEmpty()) {
                try {
                    fileService.copy(photo, "client_" + client.getIdClient());
                    client.setPhoto("client_" + client.getIdClient());
                } catch (IOException ignored) {
                    error = true;
                    flash.addFlashAttribute("errors", "No se ha podido subir la foto");
                }
            }
            if (!error) {
                if (client.getSurname2().isEmpty()) {
                    client.setSurname2(null);
                }
                if (client.getBusinessName().isEmpty()) {
                    client.setBusinessName(null);
                }
                try {
                    clientService.save(client);
                    flash.addFlashAttribute("success", "El cliente se ha guardado correctamente");
                } catch (Exception e) {
                    error = true;
                    flash.addFlashAttribute("errors", "El cliente no se ha podido guardar");
                }
            }
        }
        if (error) {
            destination = "admin/client/add";
        } else {
            destination = "redirect:/admin/client/add";
        }
        return destination;
    }

    @GetMapping(path = "/edit/{id}")
    public String edit(@PathVariable String id, Model model) {
        oldClient = clientService.get(id);
        if (oldClient != null) {
            model.addAttribute("client", oldClient);
            return "admin/client/edit";
        } else {
            return "error/404";
        }
    }

    @PostMapping(path = "/edit")
    public String edit(@Valid Client client, BindingResult result, Model model,
                       @RequestParam("file") MultipartFile photo, RedirectAttributes flash) {
        String destination;
        client.setIdClient(oldClient.getIdClient());
        client.setId(oldClient.getId());
        boolean error = false;
        if (client.getNif() != null) {
            Client temp = clientService.getByNif(client.getNif());
            if (temp != null && !temp.getIdClient().equals(client.getIdClient())) {
                result.addError(new FieldError("client", "nif",
                        "Ya existe un cliente con este nif"));
            }
        }
        if (result.hasErrors()) {
            error = true;
        } else {
            if (!photo.isEmpty()) {
                try {
                    fileService.copy(photo, "client_" + client.getIdClient());
                    client.setPhoto("client_" + client.getIdClient());
                } catch (IOException ignored) {
                    error = true;
                    flash.addFlashAttribute("errors", "No se ha podido subir la foto");
                }
            } else {
                client.setPhoto(oldClient.getPhoto());
            }
            if (!error) {
                if (client.getSurname2().isEmpty()) {
                    client.setSurname2(null);
                }
                if (client.getBusinessName().isEmpty()) {
                    client.setBusinessName(null);
                }
                try {
                    clientService.update(client);
                    flash.addFlashAttribute("success", "El cliente se ha actualizado correctamente");
                    oldClient = null;
                } catch (Exception e) {
                    error = true;
                    flash.addFlashAttribute("errors", "El cliente no se ha podido actualizar");
                }
            }
        }
        if (error) {
            model.addAttribute("client", client);
            destination = "/admin/client/edit";
        } else {
            destination = "redirect:/admin/client/get/" + client.getIdClient();
        }
        return destination;
    }

    @GetMapping("/list")
    public String list(Model model) {
        List<Client> clients = clientService.list();
        if (clients == null)
            clients = new ArrayList<>();
        model.addAttribute("clients", clients);
        return "/admin/client/list";
    }

    @GetMapping(path = "/get/{id}")
    public String get(@PathVariable String id, Model model) {
        Client client = clientService.get(id);
        String destination = "/error/404";
        if (client != null) {
            client.setUser(userService.getByIdClient(client.getIdClient()));
            model.addAttribute("client", client);
            destination = "admin/client/get";
        }
        return destination;
    }
}
