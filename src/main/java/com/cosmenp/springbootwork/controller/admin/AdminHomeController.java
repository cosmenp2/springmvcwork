package com.cosmenp.springbootwork.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/admin")
public class AdminHomeController {

    @GetMapping(path = "/")
    public String add(Model model) {
        return "admin/index";
    }
}
