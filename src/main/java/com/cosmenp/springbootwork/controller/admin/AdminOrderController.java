package com.cosmenp.springbootwork.controller.admin;

import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.service.impl.FileService;
import com.cosmenp.springbootwork.service.impl.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/admin/order")
public class AdminOrderController {
    @Autowired
    OrderService orderService;
    @Autowired
    FileService fileService;

    @GetMapping("/list")
    public String list(Model model) {
        List<Order> orders = orderService.list();
        if (orders == null)
            orders = new ArrayList<>();
        model.addAttribute("orders", orders);
        return "admin/order/list";
    }

    @GetMapping(path = "/get/{idOrder}")
    public String get(@PathVariable String idOrder, Model model) {
        Order order = orderService.getByIdOrder(idOrder);
        String destination = "/error/404";
        if (order != null) {
            model.addAttribute("order", order);
            destination = "admin/order/get";
        }
        return destination;
    }
}
