package com.cosmenp.springbootwork.controller.admin;

import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.entity.User;
import com.cosmenp.springbootwork.reportService.OrderReportService;
import com.cosmenp.springbootwork.service.impl.OrderService;
import com.cosmenp.springbootwork.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "admin/rest")
public class AdminOrderRestController {
    @Autowired
    OrderService orderService;
    @Autowired
    UserService userService;
    @Autowired
    OrderReportService orderReportService;

    @GetMapping(path = "/report/{idOrder}")
    public ResponseEntity<byte[]> report(@PathVariable String idOrder) {
        Order order = orderService.getByIdOrder(idOrder);
        if (order != null) {
            byte[] bytes = orderReportService.generateAdminReportFromJasper(orderService.getByIdOrder(idOrder).getOrderLines());
            return ResponseEntity
                    .ok()
                    // Specify content type as PDF
                    .header("Content-Type", "application/pdf; charset=UTF-8")
                    // Tell browser to display PDF if it can
                    .header("Content-Disposition", "inline; filename=\"" + idOrder + "_cosmenp.pdf\"")
                    .body(bytes);

        }
        String body = "error 404";
        return ResponseEntity.ok().body(body.getBytes());
    }
}
