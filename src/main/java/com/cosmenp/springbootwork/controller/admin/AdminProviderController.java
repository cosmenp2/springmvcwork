package com.cosmenp.springbootwork.controller.admin;

import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.entity.OrderLine;
import com.cosmenp.springbootwork.model.entity.Provider;
import com.cosmenp.springbootwork.service.impl.FileService;
import com.cosmenp.springbootwork.service.impl.OrderService;
import com.cosmenp.springbootwork.service.impl.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/admin/provider")
public class AdminProviderController {
    @Autowired
    ProviderService providerService;
    @Autowired
    OrderService orderService;
    @Autowired
    FileService fileService;

    Provider oldProvider;

    @GetMapping(path = "/add")
    public String add(Model model) {
        model.addAttribute("provider", new Provider());
        return "admin/provider/add";
    }

    @PostMapping(path = "/add")
    public String add(@Valid Provider provider, BindingResult result, Model model,
                      @RequestParam("file") MultipartFile photo, RedirectAttributes flash) {
        String destination;
        boolean error = false;
        if (provider.getCif() != null) {
            if (provider.getCif().length() != 9) {
                result.addError(new FieldError("provider", "cif",
                        "El cif debe tener 9 caracteres"));
            } else {
                Provider temp = providerService.getByCif(provider.getCif());
                if (temp != null) {
                    result.addError(new FieldError("provider", "cif",
                            "Ya existe un proveedor con este cif"));
                }
            }
        }
        if (result.hasErrors()) {
            error = true;
        } else {
            provider.setIdProvider(providerService.newCode());
            if (!photo.isEmpty()) {
                try {
                    fileService.copy(photo, "provider_" + provider.getIdProvider());
                    provider.setPhoto("provider_" + provider.getIdProvider());
                } catch (IOException ignored) {
                    error = true;
                    flash.addFlashAttribute("errors", "No se ha podido subir la foto");
                }
            }
            if (!error) {
                provider.setActive(1);
                if (provider.getCif().isEmpty()) {
                    provider.setCif(null);
                }
                try {
                    providerService.save(provider);
                    flash.addFlashAttribute("success", "El proveedor se ha guardado correctamente");
                } catch (Exception e) {
                    error = true;
                    flash.addFlashAttribute("errors", "El proveedor no se ha podido guardar");
                }
            }
        }
        if (error) {
            destination = "admin/provider/add";
        } else {
            destination = "redirect:/admin/provider/add";
        }
        return destination;
    }

    @GetMapping(path = "/edit/{id}")
    public String edit(@PathVariable String id, Model model) {
        oldProvider = providerService.get(id);
        if (oldProvider != null) {
            model.addAttribute("provider", oldProvider);
            return "admin/provider/edit";
        } else {
            return "error/404";
        }
    }

    @PostMapping(path = "/edit")
    public String edit(@Valid Provider provider, BindingResult result, Model model,
                       @RequestParam("file") MultipartFile photo, RedirectAttributes flash) {
        String destination;
        provider.setIdProvider(oldProvider.getIdProvider());
        provider.setId(oldProvider.getId());
        boolean error = false;
        if (provider.getCif() != null) {
            if (provider.getCif().length() != 9) {
                result.addError(new FieldError("provider", "cif",
                        "El cif debe tener 9 caracteres"));
            } else {
                Provider temp = providerService.getByCif(provider.getCif());
                if (temp != null && !temp.getIdProvider().equals(provider.getIdProvider())) {
                    result.addError(new FieldError("provider", "cif",
                            "Ya existe un proveedor con este cif"));
                }
            }
        }
        if (result.hasErrors()) {
            error = true;
        } else {
            if (!photo.isEmpty()) {
                try {
                    fileService.copy(photo, "provider_" + provider.getIdProvider());
                    provider.setPhoto("provider_" + provider.getIdProvider());
                } catch (IOException ignored) {
                    error = true;
                    flash.addFlashAttribute("errors", "No se ha podido subir la foto");
                }
            } else {
                provider.setPhoto(oldProvider.getPhoto());
            }
            if (!error) {
                provider.setActive(oldProvider.getActive());
                if (provider.getCif().isEmpty()) {
                    provider.setCif(null);
                }
                try {
                    providerService.update(provider);
                    flash.addFlashAttribute("success", "El proveedor se ha actualizado correctamente");
                    oldProvider = null;
                } catch (Exception e) {
                    error = true;
                    flash.addFlashAttribute("errors", "El proveedor no se ha podido actualizar");
                }
            }
        }
        if (error) {
            model.addAttribute("provider", provider);
            destination = "/admin/provider/edit";
        } else {
            destination = "redirect:/admin/provider/get/" + provider.getIdProvider();
        }
        return destination;
    }

    @GetMapping("/list")
    public String list(Model model) {
        List<Provider> providers = providerService.list();
        if (providers == null)
            providers = new ArrayList<>();
        model.addAttribute("providers", providers);
        return "/admin/provider/list";
    }

    @GetMapping(path = "/get/{id}")
    public String get(@PathVariable String id, Model model) {
        Provider provider = providerService.get(id);
        String destination = "/error/404";
        if (provider != null) {
            List<Order> orders = orderService.listByProvider(id);
            if (orders == null) {
                orders = new ArrayList<>();
            }
            double totalSold = 0.0;
            int unitsSold = 0;
            for (Order order : orders) {
                for (OrderLine line : order.getOrderLines()) {
                    if (line.getArticle().getProvider().getIdProvider().equals(id)) {
                        totalSold = Math.rint((line.getTotalLine() + totalSold) * 100) / 100;
                        unitsSold += line.getUnits();
                    }
                }
            }
            model.addAttribute("orders", orders);
            model.addAttribute("totalSold", totalSold);
            model.addAttribute("unitsSold", unitsSold);
            model.addAttribute("provider", provider);
            destination = "admin/provider/get";
        }
        return destination;
    }

    @GetMapping(path = "/disable/{id}")
    public String disable(@PathVariable String id, RedirectAttributes flash) {
        disableProvider(id, flash);
        return "redirect:/admin/provider/list";
    }

    @GetMapping(path = "/get/disable/{id}")
    public String disableOnGet(@PathVariable String id, RedirectAttributes flash) {
        disableProvider(id, flash);
        return "redirect:/admin/provider/get/" + id;
    }

    private void disableProvider(@PathVariable String id, RedirectAttributes flash) {
        Provider provider = providerService.get(id);
        if (provider != null) {
            providerService.disable(id);
            flash.addFlashAttribute("success", "El proveedor " +
                    provider.getIdProvider() + " se ha desactivado correctamente");
        } else {
            flash.addFlashAttribute("errors", "El proveedor " + id + " no se ha podido desactivar correctamente");
        }
    }

    @GetMapping(path = "/enable/{id}")
    public String enable(@PathVariable String id, RedirectAttributes flash) {
        enableProvider(id, flash);
        return "redirect:/admin/provider/list";
    }

    @GetMapping(path = "/get/enable/{id}")
    public String enableOnGet(@PathVariable String id, RedirectAttributes flash) {
        enableProvider(id, flash);
        return "redirect:/admin/provider/get/" + id;
    }

    private void enableProvider(@PathVariable String id, RedirectAttributes flash) {
        Provider provider = providerService.get(id);
        if (provider != null) {
            providerService.enable(id);
            flash.addFlashAttribute("success", "El proveedor " +
                    provider.getIdProvider() + " se ha activado correctamente");
        } else {
            flash.addFlashAttribute("errors", "El proveedor " + id + " no se ha podido activar correctamente");
        }
    }
}
