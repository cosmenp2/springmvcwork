package com.cosmenp.springbootwork.controller.admin;

import com.cosmenp.springbootwork.model.entity.User;
import com.cosmenp.springbootwork.service.impl.RoleService;
import com.cosmenp.springbootwork.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/admin/user")
public class AdminUserController {
    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping(path = "/add")
    public String add(Model model) {
        model.addAttribute("user", new User());
        return "admin/user/add";
    }

    @PostMapping(path = "/add")
    public String add(@Valid User user, BindingResult result, Model model, RedirectAttributes flash) {
        String destination;
        boolean error = false;
        if (user.getUsername() != null) {
            User temp = userService.getByUsername(user.getUsername());
            if (temp != null) {
                result.addError(new FieldError("user", "username",
                        "Ya existe un usuario con este nombre de usuario"));
            }
        }
        if (user.getEmail() != null) {
            User temp = userService.getByEmail(user.getEmail());
            if (temp != null) {
                result.addError(new FieldError("user", "email",
                        "Ya existe un usuario con este correo electrónico"));
            }
        }

        if (result.hasErrors()) {
            error = true;
        } else {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setClient(null);
            user.setRole(roleService.findByRoleName("ADMIN"));
            user.setActive(1);
            try {
                userService.save(user);
                flash.addFlashAttribute("success", "El usuario se ha guardado correctamente");
            } catch (Exception e) {
                error = true;
                flash.addFlashAttribute("errors", "El usuario no se ha podido guardar");
            }
        }
        if (error) {
            destination = "admin/user/add";
        } else {
            destination = "redirect:/admin/user/add";
        }
        return destination;
    }

    @GetMapping("/list")
    public String list(Model model) {
        List<User> users = userService.list();
        if (users == null)
            users = new ArrayList<>();
        model.addAttribute("users", users);
        model.addAttribute("actualUser", getActualUser());
        return "/admin/user/list";
    }

    @GetMapping(path = "/get/{username}")
    public String get(@PathVariable String username, Model model) {
        if (username.equals(getActualUser().getUsername())) {
            return "redirect:/admin/user/getProfile";
        }
        User user = userService.getByUsername(username);
        model.addAttribute("user", user);
        return "admin/user/get";
    }

    @GetMapping(path = "/disable/{username}")
    public String disable(@PathVariable String username, RedirectAttributes flash) {
        disableUser(username, flash);
        return "redirect:/admin/user/list";
    }

    @GetMapping(path = "/get/disable/{username}")
    public String disableOnGet(@PathVariable String username, RedirectAttributes flash) {
        disableUser(username, flash);
        return "redirect:/admin/user/get/" + username;
    }

    private void disableUser(@PathVariable String username, RedirectAttributes flash) {
        User user = userService.getByUsername(username);
        if (user != null) {
            User actualUser = getActualUser();
            if (actualUser.getUsername().equals(username)) {
                flash.addFlashAttribute("errors", "¡¡No se puede desactivarse a si mismo!!");
            } else {
                userService.disable(username);
                flash.addFlashAttribute("success", "El usuario " +
                        user.getUsername() + " se ha desactivado correctamente");
            }
        } else {
            flash.addFlashAttribute("errors", "El usuario " + username + " no se ha podido desactivar correctamente");
        }
    }

    @GetMapping(path = "/enable/{username}")
    public String enable(@PathVariable String username, RedirectAttributes flash) {
        enableUser(username, flash);
        return "redirect:/admin/user/list";
    }

    @GetMapping(path = "/get/enable/{username}")
    public String enableOnGet(@PathVariable String username, RedirectAttributes flash) {
        enableUser(username, flash);
        return "redirect:/admin/user/get/" + username;
    }

    private void enableUser(@PathVariable String username, RedirectAttributes flash) {
        User user = userService.getByUsername(username);
        if (user != null) {
            userService.enable(username);
            flash.addFlashAttribute("success", "El usuario " +
                    user.getUsername() + " se ha activado correctamente");
        } else {
            flash.addFlashAttribute("errors", "El usuario " + username + " no se ha podido activar correctamente");
        }
    }

    @GetMapping(path = "/getProfile")
    public String getProfile(Model model) {
        User user = getActualUser();
        model.addAttribute("user", user);
        return "admin/user/getProfile";
    }

    @GetMapping(path = "/changeEmail")
    public String changeEmail() {
        return "admin/user/changeEmail";
    }

    @PostMapping(path = "/changeEmail")
    public String changeEmail(String pass, String mail, RedirectAttributes flash) {
        String destination;
        boolean error = false;
        User user = getActualUser();
        if (passwordEncoder.matches(pass, user.getPassword())) {

            User temp = userService.getByEmail(user.getEmail());
            if (temp != null && !temp.getEmail().equals(user.getEmail())) {
                flash.addFlashAttribute("errors",
                        "Ya existe un usuario con este correo electrónico");
                error = true;
            } else {
                user.setEmail(mail);
                try {
                    userService.update(user);
                    flash.addFlashAttribute("success",
                            "El correo se ha actualizado correctamente.");
                } catch (Exception e) {
                    error = true;
                    flash.addFlashAttribute("errors",
                            "Ha habido un fallo al actualizar su correo, " +
                                    "intentelo de nuevo más tarde");
                }
            }
        } else {
            error = true;
            flash.addFlashAttribute("errors",
                    "Contraseña incorrecta");
        }
        if (error) {
            destination = "redirect:/admin/user/changeEmail";
        } else {
            destination = "redirect:/admin/user/getProfile";
        }
        return destination;
    }

    private User getActualUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userDetails = (UserDetails) principal;
        return userService.getByUsername(userDetails.getUsername());
    }
}
