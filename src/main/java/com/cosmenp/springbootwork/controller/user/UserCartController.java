package com.cosmenp.springbootwork.controller.user;

import com.cosmenp.springbootwork.model.entity.Article;
import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.entity.OrderLine;
import com.cosmenp.springbootwork.model.entity.User;
import com.cosmenp.springbootwork.service.impl.ArticleService;
import com.cosmenp.springbootwork.service.impl.OrderService;
import com.cosmenp.springbootwork.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@SessionScope
@RequestMapping(path = "/user/cart")
public class UserCartController {
    @Autowired
    UserService userService;
    @Autowired
    OrderService orderService;
    @Autowired
    ArticleService articleService;

    List<OrderLine> orderLines;
    Order order;
    boolean procesing = false;
    String tempOrder;

    @GetMapping(path = "/list")
    public String list(Model model, RedirectAttributes flash) {
        if (isCartOutdated()) {
            flash.addFlashAttribute("warning",
                    "La disponibilidad de los artículos ha cambiado, " +
                            "revise su cesta de la compra antes de continuar.");
        }
        double total = 0;
        for (OrderLine line : orderLines) {
            total = Math.rint((line.getTotalLine() + total) * 100) / 100;
        }
        model.addAttribute("lines", getOrderLines());
        model.addAttribute("total", total);
        return "user/cart/list";
    }

    private boolean isCartOutdated() {
        boolean outdate = false;
        if (getOrderLines().size() > 0) {
            for (OrderLine line : orderLines) {
                Article article = articleService.get(line.getArticle().getIdArticle());
                if (article.getStock() < line.getUnits()) {
                    outdate = true;
                    line.setUnits(article.getStock());
                }
                if (line.getUnits() == 0) {
                    orderLines.remove(line);
                    outdate = true;
                }
            }
        }
        return outdate;
    }

    @GetMapping(path = "/add/{idArticle}")
    public String add(@PathVariable String idArticle, RedirectAttributes flash) {
        processAdd(idArticle, flash);
        return "redirect:/user/cart/list";
    }

    @GetMapping(path = "/addFromCatalogue/{idArticle}")
    public String addFromCatalogue(@PathVariable String idArticle, RedirectAttributes flash) {
        processAdd(idArticle, flash);
        return "redirect:/public/article/list";
    }

    @GetMapping(path = "/addFromViewArticle/{idArticle}")
    public String addFromViewArticle(@PathVariable String idArticle, RedirectAttributes flash) {
        processAdd(idArticle, flash);
        return "redirect:/public/article/get/" + idArticle;
    }

    private void processAdd(@PathVariable String idArticle, RedirectAttributes flash) {
        Article article = articleService.getActive(idArticle);
        if (article != null) {
            if (addArticleToCart(article)) {
                flash.addFlashAttribute("success",
                        "Se ha añadido el artículo.");
            } else {
                flash.addFlashAttribute("errors",
                        "No hay mas unidades del producto disponibles");
            }
        } else {
            flash.addFlashAttribute("errors",
                    "El artículo no existe");
        }
    }

    private boolean addArticleToCart(Article article) {
        int position = searchArticleInOrders(article);
        boolean result = false;
        if (position != -1) {
            if (orderLines.get(position).getUnits() + 1 <= article.getStock()) {
                orderLines.get(position).setUnits(orderLines.get(position).getUnits() + 1);
                result = true;
            } else if (orderLines.get(position).getUnits() > article.getStock()) {
                orderLines.get(position).setUnits(article.getStock());
            }
        } else {
            OrderLine orderLine = new OrderLine(article.getName(), article.getManufacture(),
                    article.getDescription(), article.getPvp(), article.getIva(), 1);
            orderLine.setArticle(article);
            orderLines.add(orderLine);
            result = true;
        }
        return result;
    }

    private int searchArticleInOrders(Article article) {
        int position = 0;
        boolean founded = false;
        while (position < getOrderLines().size() && !founded) {
            if (orderLines.get(position).getArticle().getIdArticle().equals(article.getIdArticle())) {
                founded = true;
            } else {
                position++;
            }
        }
        if (!founded) {
            position = -1;
        }
        return position;
    }

    @GetMapping(path = "/minus/{idArticle}")
    public String minus(@PathVariable String idArticle, RedirectAttributes flash) {
        processMinus(idArticle, flash);
        return "redirect:/user/cart/list";
    }

    private void processMinus(@PathVariable String idArticle, RedirectAttributes flash) {
        Article article = articleService.get(idArticle);
        if (article != null) {
            if (OneArticleLessToCart(article)) {
                flash.addFlashAttribute("success",
                        "Se ha retirado una unidad del artículo.");
            } else {
                flash.addFlashAttribute("warning",
                        "El artículo no esta en la cesta de la compra");
            }
        } else {
            flash.addFlashAttribute("errors",
                    "El artículo no existe");
        }
    }

    private boolean OneArticleLessToCart(Article article) {
        int position = searchArticleInOrders(article);
        boolean result = false;
        if (position != -1) {
            if (orderLines.get(position).getUnits() - 1 == 0) {
                orderLines.remove(position);
            } else {
                orderLines.get(position).setUnits(orderLines.get(position).getUnits() - 1);
            }
            result = true;
        }
        return result;
    }

    @GetMapping(path = "/remove/{idArticle}")
    public String delete(@PathVariable String idArticle, RedirectAttributes flash) {
        processDelete(idArticle, flash);
        return "redirect:/user/cart/list";
    }

    private void processDelete(@PathVariable String idArticle, RedirectAttributes flash) {
        Article article = new Article();
        article.setIdArticle(idArticle);
        if (deleteArticleFromCart(article)) {
            flash.addFlashAttribute("success",
                    "Se ha quitado el artículo de su cesta de la compra.");
        } else {
            flash.addFlashAttribute("warning",
                    "El artículo no esta en la cesta de la compra");
        }
    }

    private boolean deleteArticleFromCart(Article article) {
        int position = searchArticleInOrders(article);
        boolean result = false;
        if (position != -1) {
            result = true;
            orderLines.remove(position);
        }
        return result;
    }

    @GetMapping(path = "/buy")
    public String buy() {
        if (procesing) {
            try {
                revertStockUpdate();
                procesing = false;
            } catch (Exception e) {
                System.err.println("Fallo al revertir el stock en la bbdd");
                /* Aqui habria un control de errores estrepitoso que guardase un log con las lineas del pedido y sus unidades
                para evitar inconsistencia en la bbdd (Siempre se puede actualizar el stock ;) */
            }
        }
        return "user/cart/buy";
    }

    private void revertStockUpdate() throws Exception {
        List<Article> articles = new ArrayList<>();
        for (OrderLine line : orderLines) {
            Article article = articleService.get(line.getArticle().getIdArticle());
            article.setStock(article.getStock() + line.getUnits());
            articles.add(article);
        }
        articleService.update(articles);
    }

    @PostMapping(path = "/buy")
    public String buy(String method, RedirectAttributes flash) {
        String destination = "redirect:/user/cart/buySuccessful";
        if (isCartOutdated()) {
            flash.addFlashAttribute("warning",
                    "La disponibilidad de los artículos ha cambiado, " +
                            "revise su cesta de la compra antes de continuar.");
            destination = "redirect:/user/cart/list";
        } else if (getOrderLines().size() != 0) {
            try {
                updateStock();
                procesing = true;
                order = new Order();
                order.setPayMethod(method);
                saveOrder();
                tempOrder = order.getIdOrder();
            } catch (Exception e) {
                flash.addFlashAttribute("errors",
                        "Ha habido un error al procesar la compra. Intentelo de nuevo más tarde");
                destination = "redirect:/user/cart/buy";
            }
        } else {
            flash.addFlashAttribute("errors",
                    "No tiene ningún artículo en su cesta.");
            destination = "redirect:/user/cart/list";
        }
        return destination;
    }

    private void updateStock() throws Exception {
        List<Article> articles = new ArrayList<>();
        for (OrderLine line : orderLines) {
            Article article = articleService.get(line.getArticle().getIdArticle());
            article.setStock(article.getStock() - line.getUnits());
            articles.add(article);
        }
        articleService.update(articles);
    }

    private void saveOrder() {
        User user = getActualUser();
        order.setClient(user.getClient());
        order.setIdOrder(orderService.newCode(user.getClient().getPostalCode()));
        order.setDate(new Date());
        order.setActive(1);
        order.setPaid(1);
        for (OrderLine line: orderLines){
            line.setOrder(order);
        }
        order.setOrderLines(orderLines);
        orderService.save(order);
        procesing = false;
        order = null;
        orderLines = new ArrayList<>();
    }

    private User getActualUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userDetails = (UserDetails) principal;
        return userService.getByUsername(userDetails.getUsername());
    }

    @GetMapping(path = "/buySuccessful")
    public String buySuccessful(Model model) {
        String destination = "redirect:/user/order/list";
        if (tempOrder != null) {
            if (!tempOrder.isEmpty()) {
                destination = "user/cart/buySuccessful";
                model.addAttribute("order", tempOrder);
                tempOrder = null;
            }
        }
        return destination;
    }

    private List<OrderLine> getOrderLines() {
        if (orderLines == null) {
            orderLines = new ArrayList<>();
        }
        return orderLines;
    }
}
