package com.cosmenp.springbootwork.controller.user;

import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.entity.User;
import com.cosmenp.springbootwork.service.impl.FileService;
import com.cosmenp.springbootwork.service.impl.OrderService;
import com.cosmenp.springbootwork.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/user/order")
public class UserOrderController {
    @Autowired
    UserService userService;
    @Autowired
    OrderService orderService;
    @Autowired
    FileService fileService;

    @GetMapping("/list")
    public String list(Model model) {
        List<Order> orders = orderService.listByClient(getActualUser().getClient().getIdClient());
        if (orders == null)
            orders = new ArrayList<>();
        model.addAttribute("orders", orders);
        return "user/order/list";
    }

    @GetMapping(path = "/get/{idOrder}")
    public String get(@PathVariable String idOrder, Model model) {
        Order order = orderService.getByIdOrder(idOrder);
        String destination = "/error/404";
        if (order != null) {
            if (order.getClient().getIdClient().equals(getActualUser().getClient().getIdClient())) {
                model.addAttribute("order", order);
                destination = "user/order/get";
            }
        }
        return destination;
    }

    private User getActualUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userDetails = (UserDetails) principal;
        return userService.getByUsername(userDetails.getUsername());
    }
}
