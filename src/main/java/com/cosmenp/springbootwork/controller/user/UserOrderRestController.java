package com.cosmenp.springbootwork.controller.user;

import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.entity.User;
import com.cosmenp.springbootwork.reportService.OrderReportService;
import com.cosmenp.springbootwork.service.impl.OrderService;
import com.cosmenp.springbootwork.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping (path = "user/rest")
public class UserOrderRestController {
    @Autowired
    OrderService orderService;
    @Autowired
    UserService userService;
    @Autowired
    OrderReportService orderReportService;

    @GetMapping(path = "/report/{idOrder}")
    public ResponseEntity<byte[]> report(@PathVariable String idOrder) {
        Order order = orderService.getByIdOrder(idOrder);
        if (order != null) {
            if (order.getClient().getIdClient().equals(getActualUser().getClient().getIdClient())) {
                byte[] bytes = orderReportService.generateReportFromJasper(orderService.getByIdOrder(idOrder).getOrderLines());
                return ResponseEntity
                        .ok()
                        // Specify content type as PDF
                        .header("Content-Type", "application/pdf; charset=UTF-8")
                        // Tell browser to display PDF if it can
                        .header("Content-Disposition", "inline; filename=\"" + idOrder + "_cosmenp.pdf\"")
                        .body(bytes);
            }
        }
        String body = "error 404";
        return ResponseEntity.ok().body(body.getBytes());
    }

    private User getActualUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userDetails = (UserDetails) principal;
        return userService.getByUsername(userDetails.getUsername());
    }
}
