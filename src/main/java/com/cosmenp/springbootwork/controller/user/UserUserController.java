package com.cosmenp.springbootwork.controller.user;

import com.cosmenp.springbootwork.model.entity.Client;
import com.cosmenp.springbootwork.model.entity.User;
import com.cosmenp.springbootwork.service.impl.ClientService;
import com.cosmenp.springbootwork.service.impl.FileService;
import com.cosmenp.springbootwork.service.impl.RoleService;
import com.cosmenp.springbootwork.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping(path = "/user/user")
public class UserUserController {
    @Autowired
    UserService userService;
    @Autowired
    FileService fileService;
    @Autowired
    RoleService roleService;
    @Autowired
    ClientService clientService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping(path = "/edit")
    public String edit(Model model) {
        User user = getActualUser();
        model.addAttribute("user", user);
        model.addAttribute("client", user.getClient());
        return "user/user/edit";
    }

    @PostMapping(path = "/edit")
    public String edit(@Valid User user, BindingResult resultUser, @Valid Client client, BindingResult resultClient,
                       Model model, @RequestParam("file") MultipartFile photo, RedirectAttributes flash) {
        String destination;
        User oldUser = getActualUser();
        Client oldClient = oldUser.getClient();
        boolean error = false;
        if (user.getEmail() != null) {
            User temp = userService.getByEmail(user.getEmail());
            if (temp != null && !temp.getEmail().equals(oldUser.getEmail())) {
                resultUser.addError(new FieldError("user", "email",
                        "Ya existe un usuario con este correo electrónico"));
            }
        }

        if (client.getNif() != null) {
            Client temp = clientService.getByNif(client.getNif());
            if (temp != null && !temp.getNif().equalsIgnoreCase(oldClient.getNif())) {
                resultClient.addError(new FieldError("client", "client.nif",
                        "Ya existe un cliente con este nif"));
            }
        }

        if (resultUser.getErrorCount() > 2 || resultClient.hasErrors()) {
            error = true;
        } else {
            if (!photo.isEmpty()) {
                try {
                    fileService.copy(photo, "client_" + oldClient.getIdClient());
                    oldClient.setPhoto("client_" + oldClient.getIdClient());
                } catch (IOException ignored) {
                    error = true;
                    flash.addFlashAttribute("errors", "No se ha podido subir la foto");
                }
            }
            if (client.getSurname2().isEmpty()) {
                oldClient.setSurname2(null);
            } else {
                oldClient.setSurname2(client.getSurname2());
            }
            if (client.getBusinessName().isEmpty()) {
                oldClient.setBusinessName(null);
            } else {
                oldClient.setBusinessName(client.getBusinessName());
            }

            oldUser.setEmail(user.getEmail());
            oldClient.setName(client.getName());
            oldClient.setSurname1(client.getSurname1());
            oldClient.setNif(oldClient.getNif());
            oldClient.setPostalCode(client.getPostalCode());
            oldClient.setAddress(client.getAddress());
            oldUser.setClient(oldClient);

            try {
                userService.update(oldUser);
                flash.addFlashAttribute("success",
                        "Se ha actualizado correctamente sus datos.");
            } catch (Exception e) {
                error = true;
                flash.addFlashAttribute("errors",
                        "Ha habido un fallo al actualizar sus datos, " + "intentelo de nuevo más tarde");
            }
        }
        if (error) {
            destination = "user/user/edit";
        } else {
            destination = "redirect:/user/user/get";
        }
        return destination;
    }

    @GetMapping(path = "/get")
    public String get(Model model) {
        User user = getActualUser();
        model.addAttribute("user", user);
        return "user/user/get";
    }

    private User getActualUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userDetails = (UserDetails) principal;
        return userService.getByUsername(userDetails.getUsername());
    }

    @GetMapping(path = "/passReset")
    public String passReset() {
        return "user/user/passReset";
    }

    @PostMapping(path = "/passReset")
    public String passReset(String oldPass, String newPass, String repPass, RedirectAttributes flash) {
        String destination;
        boolean error = false;
        boolean admin = false;
        if (newPass.equals(repPass)) {
            User user = getActualUser();
            if (passwordEncoder.matches(oldPass, user.getPassword())) {
                user.setPassword(passwordEncoder.encode(newPass));
                try {
                    userService.update(user);
                    flash.addFlashAttribute("success",
                            "La contraseña se ha cambiado correctamente.");
                    if (user.getRole().getRole().equalsIgnoreCase("ADMIN"))
                        admin = true;
                } catch (Exception e) {
                    error = true;
                    flash.addFlashAttribute("errors",
                            "Ha habido un fallo al actualizar su contraseña, " +
                                    "intentelo de nuevo más tarde");
                }
            } else {
                error = true;
                flash.addFlashAttribute("errors",
                        "La contraseña antigua es incorrecta");
            }
        } else {
            error = true;
            flash.addFlashAttribute("errors",
                    "Las contraseñas no coinciden");
        }

        if (error) {
            destination = "redirect:/user/user/passReset";
        } else {
            if (admin)
                destination = "redirect:/admin/user/getProfile";
            else
                destination = "redirect:/user/user/get";
        }
        return destination;
    }
}
