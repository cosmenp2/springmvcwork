package com.cosmenp.springbootwork.model.entity;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "spring_articles")
public class Article implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, unique = true, length = 7, name = "id_article")
    private String idArticle;
    @ManyToOne(optional = false)
    @JoinColumn(name = "id_provider", referencedColumnName = "id_provider")
    private Provider provider;
    @NotEmpty(message = "Este campo es obligatorio")
    @Column(nullable = false)
    private String name;
    private String description;
    @NotEmpty(message = "Este campo es obligatorio")
    @Column(nullable = false)
    private String manufacture;
    @NotNull(message = "Este campo es obligatorio")
    @Min(value = 0, message = "Tiene que ser mayor o igual que 0")
    @Column(nullable = false)
    private Double cost;
    @NotNull(message = "Este campo es obligatorio")
    @Min(value = 0, message = "Tiene que ser mayor o igual que 0")
    @Column(nullable = false)
    private Double pvp;
    @NotNull(message = "Este campo es obligatorio")
    @Min(value = 0, message = "Tiene que ser mayor o igual que 0")
    @Column(nullable = false)
    private Double iva;
    @NotNull(message = "Este campo es obligatorio")
    @Min(value = 0, message = "Tiene que ser mayor o igual que 0")
    @Column(nullable = false)
    private Integer stock;
    private String photo;
    @Column(nullable = false)
    private int active;
    @Transient
    private double totalPrize;

    public Article() {
    }

    public Article(String idArticle, String name, String description, String manufacture, Double cost, Double pvp, Double iva, Integer stock, String photo) {
        this.idArticle = idArticle;
        this.name = name;
        this.description = description;
        this.manufacture = manufacture;
        this.cost = cost;
        this.pvp = pvp;
        this.iva = iva;
        this.stock = stock;
        this.photo = photo;
        this.active = 1;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(String idArticulo) {
        this.idArticle = idArticulo;
    }

    public String getName() {
        return name;
    }

    public void setName(String nombre) {
        this.name = nombre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String descripcion) {
        this.description = descripcion;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String fabricante) {
        this.manufacture = fabricante;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double coste) {
        this.cost = coste;
    }

    public Double getPvp() {
        return pvp;
    }

    public void setPvp(Double pvp) {
        this.pvp = pvp;
    }

    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String foto) {
        this.photo = foto;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int activo) {
        this.active = activo;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public double getTotalPrize() {
        if (pvp != null && iva != null) {
            totalPrize = Math.rint((pvp * iva / 100 + pvp) * 100) / 100;
        } else {
            totalPrize = 0;
        }
        return totalPrize;
    }
}
