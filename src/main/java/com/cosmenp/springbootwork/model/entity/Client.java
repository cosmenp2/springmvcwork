package com.cosmenp.springbootwork.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "spring_clients")
public class Client implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "id_client", unique = true, length = 15, nullable = false)
    private String idClient;
    @NotEmpty(message = "Este campo es obligatorio")
    @Column(nullable = false)
    private String name;
    @NotEmpty(message = "Este campo es obligatorio")
    @Column(nullable = false)
    private String surname1;
    private String surname2;
    @NotEmpty(message = "Este campo es obligatorio")
    @Size(min = 9, max = 9, message = "Introduzca un valor válido")
    @Column(unique = true, length = 9, nullable = false)
    private String nif;
    private String businessName;
    @NotEmpty(message = "Este campo es obligatorio")
    @Pattern(regexp = "\\d*", message = "El código postal debe ser numérico")
    @Column(nullable = false)
    private String postalCode;
    @NotEmpty(message = "Este campo es obligatorio")
    @Column(nullable = false)
    private String address;
    private String photo;
    @Transient
    private User user;

    @OneToMany(mappedBy = "client")
    private List<Order> orders;

    public Client() {
    }

    public Client(String idClient, String name, String surname1, String surname2, String nif, String businessName, String postalCode, String address, String photo) {
        this.idClient = idClient;
        this.name = name;
        this.surname1 = surname1;
        this.surname2 = surname2;
        this.nif = nif;
        this.businessName = businessName;
        this.postalCode = postalCode;
        this.address = address;
        this.photo = photo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname1() {
        return surname1;
    }

    public void setSurname1(String surname1) {
        this.surname1 = surname1;
    }

    public String getSurname2() {
        return surname2;
    }

    public void setSurname2(String surname2) {
        this.surname2 = surname2;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
