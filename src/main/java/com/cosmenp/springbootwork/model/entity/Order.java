package com.cosmenp.springbootwork.model.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "spring_orders")
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "id_order", unique = true, length = 10, nullable = false)
    private String idOrder;
    @ManyToOne(optional = false)
    @JoinColumn(name = "id_client", referencedColumnName = "id_client")
    private Client client;
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(nullable = false)
    private Integer paid;
    @Column(nullable = false)
    private String payMethod;
    @Column(nullable = false)
    private Integer active;
    @OneToMany(mappedBy = "order", cascade = CascadeType.PERSIST)
    List<OrderLine> orderLines;
    @Transient
    private Double totalPrice;


    public Order() {
    }

    public Order(String idOrder, Date date, Integer paid, String payMethod, Integer active) {
        this.idOrder = idOrder;
        this.date = date;
        this.paid = paid;
        this.payMethod = payMethod;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getPaid() {
        return paid;
    }

    public void setPaid(Integer paid) {
        this.paid = paid;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public Double getTotalPrice() {
        totalPrice = 0.0;
        for (OrderLine line : orderLines) {
            totalPrice = Math.rint((line.getTotalLine() + totalPrice) * 100) / 100;
        }
        return totalPrice;
    }
}
