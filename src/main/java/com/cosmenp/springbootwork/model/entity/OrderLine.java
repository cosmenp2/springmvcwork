package com.cosmenp.springbootwork.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "spring_order_lines")
public class OrderLine implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_order", referencedColumnName = "id_order")
    private Order order;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_article", referencedColumnName = "id_article")
    private Article article;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String manufacturer;
    private String description;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private Double iva;
    @Column(nullable = false)
    private Integer units;
    @Transient
    private double totalLine;
    @Transient
    private double totalUnit;

    public OrderLine() {
    }

    public OrderLine(String name, String manufacturer, String description, Double price, Double iva, Integer units) {
        this.name = name;
        this.manufacturer = manufacturer;
        this.description = description;
        this.price = price;
        this.iva = iva;
        this.units = units;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public double getTotalLine() {
        if (units != null) {
            if (units >= 0) {
                totalLine = Math.rint(getTotalUnit() * units * 100) / 100;
            } else {
                totalLine = 0;
            }
        } else
            totalLine = 0;
        return totalLine;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public double getTotalUnit() {
        if (price != null && iva != null) {
            totalUnit = Math.rint((price * iva / 100 + price) * 100) / 100;
        } else {
            totalUnit = 0;
        }
        return totalUnit;
    }
}
