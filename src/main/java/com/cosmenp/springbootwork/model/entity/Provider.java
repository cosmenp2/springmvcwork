package com.cosmenp.springbootwork.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "spring_providers")
public class Provider implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, unique = true, length = 4, name = "id_provider")
    private String idProvider;
    @NotEmpty(message = "Este campo es obligatorio")
    @Column(nullable = false)
    private String name;
    @NotEmpty(message = "Este campo es obligatorio")
    @Column(nullable = false)
    private String businessName;
    @Size(max = 9, message = "Introduzca un valor válido")
    @Column(unique = true, length = 9)
    private String cif;
    private String photo;
    @Column(nullable = false)
    private int active;
    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
    List<Article> articles;

    public Provider() {
    }

    public Provider(String idProvider, String name, String businessName, String cif, String photo) {
        this.idProvider = idProvider;
        this.name = name;
        this.businessName = businessName;
        this.cif = cif;
        this.photo = photo;
        this.active = 1;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(String idProveedor) {
        this.idProvider = idProveedor;
    }

    public String getName() {
        return name;
    }

    public void setName(String nombre) {
        this.name = nombre;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String razonSocial) {
        this.businessName = razonSocial;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String foto) {
        this.photo = foto;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int activo) {
        this.active = activo;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
