package com.cosmenp.springbootwork.model.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "spring_roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true, length = 25)
    private String role;
    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL)
    List<User> users;

    public Role() {
    }

    public Role(String role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
