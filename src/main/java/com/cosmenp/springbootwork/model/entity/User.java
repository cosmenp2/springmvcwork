package com.cosmenp.springbootwork.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Entity
@Table(name = "spring_users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotEmpty(message = "Este campo es obligatorio")
    @Column(unique = true, length = 30, nullable = false)
    private String username;
    @NotEmpty(message = "Este campo es obligatorio")
    @Column(nullable = false)
    private String password;
    @NotEmpty(message = "Este campo es obligatorio")
    @Column(unique = true, length = 50, nullable = false)
    private String email;
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "id_client", referencedColumnName = "id_client", unique = true)
    private Client client;
    @ManyToOne
    @JoinColumn(nullable = false, name = "id_role")
    private Role role;
    @Column(nullable = false)
    private Integer active;

    public User() {
    }

    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public boolean isActivo() {
        return getActive() == 1;
    }
}
