package com.cosmenp.springbootwork.model.repository;

import com.cosmenp.springbootwork.model.entity.Article;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ArticleRepository extends CrudRepository<Article, Integer> {
    Article findByIdArticle(String idArticle);

    @Query("SELECT max(a.idArticle) FROM Article a WHERE a.provider.idProvider = ?1")
    String getMaxCode(String idProvider);

    @Query("SELECT a FROM Article a WHERE a.active = 1 OR a.stock > 0")
    List<Article> findActive();

    @Query("SELECT a FROM Article a WHERE a.idArticle=:idArticle AND (a.active = 1 OR a.stock > 0)")
    Article findByIdArticleActive(String idArticle);
}
