package com.cosmenp.springbootwork.model.repository;

import com.cosmenp.springbootwork.model.entity.Client;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Integer> {
    Client findByIdClient(String idClient);

    @Query("SELECT max(c.idClient) FROM Client c WHERE c.postalCode = ?1")
    String getMaxCode(String postalCode);

    Client findByNif(String nif);
}
