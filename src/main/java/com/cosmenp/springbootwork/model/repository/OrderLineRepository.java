package com.cosmenp.springbootwork.model.repository;

import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.entity.OrderLine;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderLineRepository extends CrudRepository<OrderLine, Integer> {
    List<OrderLine> findByOrder(Order order);
}
