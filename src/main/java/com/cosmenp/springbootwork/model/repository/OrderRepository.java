package com.cosmenp.springbootwork.model.repository;

import com.cosmenp.springbootwork.model.entity.Client;
import com.cosmenp.springbootwork.model.entity.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Integer> {
    List<Order> findByClient(Client client);

    Order findByIdOrder(String idOrder);

    List<Order> findByPaid(int payed);

    List<Order> findByDateAfter(Date date);

    List<Order> findByDateBefore(Date date);

    List<Order> findByDateBetween(Date before, Date after);

    @Query("SELECT o from Order o INNER JOIN OrderLine ol ON o.idOrder = ol.order " +
            "WHERE ol.article.idArticle=?1 GROUP BY o.idOrder")
    List<Order> findOrdersByIdArticle(String idArticle);

    @Query("SELECT o from Order o INNER JOIN OrderLine ol ON o.idOrder = ol.order.idOrder " +
            "INNER JOIN Article a ON ol.article.idArticle=a.idArticle " +
            "WHERE a.provider.idProvider= ?1 GROUP BY o.idOrder")
    List<Order> findByIdProvider(String idProvider);

    @Query("SELECT max(o.idOrder) FROM Order o WHERE o.idOrder like CONCAT(?1,'%') ")
    String getMaxCode(String postalCode);
}
