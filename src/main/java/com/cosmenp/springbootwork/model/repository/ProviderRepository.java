package com.cosmenp.springbootwork.model.repository;

import com.cosmenp.springbootwork.model.entity.Provider;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProviderRepository extends CrudRepository<Provider, Integer> {
    Provider findByIdProvider(String idProvider);

    @Query("SELECT max(p.idProvider) FROM Provider p")
    String getMaxCode();

    List<Provider> findByActiveEquals(int active);

    Provider findByCif(String cif);
}
