package com.cosmenp.springbootwork.model.repository;

import com.cosmenp.springbootwork.model.entity.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role findByRole(String role);
}
