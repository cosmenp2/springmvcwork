package com.cosmenp.springbootwork.model.repository;

import com.cosmenp.springbootwork.model.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String username);

    User findByEmail(String email);

    User findByClient_IdClient(String idClient);
}
