package com.cosmenp.springbootwork.reportService;

import com.cosmenp.springbootwork.model.entity.OrderLine;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderReportService {
    public byte[] generateReportFromJasper(List<OrderLine> orderLines) {
        try {
            String reportPath=System.getProperty("user.home") + "/pdf_cosmenp";
            try {
                Files.createDirectory(Paths.get(reportPath));
            } catch (Exception ignored){}
            reportPath += "/";

            // Carga DataSource
            JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(orderLines);
            // Añade parámetros
            Map<String, Object> parameters = new HashMap<>();
//			parameters.put("fechaListado", new Date());
            File fileReport2 = ResourceUtils.getFile("classpath:report/user_buy.jasper");
            JasperPrint jasperPrint = JasperFillManager.fillReport(fileReport2.getAbsolutePath(), parameters,jrBeanCollectionDataSource);
            // Exporta en pdf
            JasperExportManager.exportReportToPdfFile(jasperPrint, reportPath +
                    orderLines.get(0).getOrder().getIdOrder()+ ".pdf");
            return JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] generateAdminReportFromJasper(List<OrderLine> orderLines) {
        try {
            String reportPath=System.getProperty("user.home") + "/pdf_cosmenp";
            try {
                Files.createDirectory(Paths.get(reportPath));
            } catch (Exception ignored){}
            reportPath += "/";

            // Carga DataSource
            JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(orderLines);
            // Añade parámetros
            Map<String, Object> parameters = new HashMap<>();
//			parameters.put("fechaListado", new Date());
            File fileReport2 = ResourceUtils.getFile("classpath:report/admin_order.jasper");
            JasperPrint jasperPrint = JasperFillManager.fillReport(fileReport2.getAbsolutePath(), parameters,jrBeanCollectionDataSource);
            // Exporta en pdf
            JasperExportManager.exportReportToPdfFile(jasperPrint, reportPath +
                    orderLines.get(0).getOrder().getIdOrder()+ "_admin.pdf");
            return JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
