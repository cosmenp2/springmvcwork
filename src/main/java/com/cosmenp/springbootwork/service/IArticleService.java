package com.cosmenp.springbootwork.service;

import com.cosmenp.springbootwork.model.entity.Article;

import java.util.List;

public interface IArticleService {
    void save(Article article) throws Exception;

    void update(Article article) throws Exception;

    List<Article> list();

    Article get(String idArticle);

    void disable(String idArticle);

    void enable(String idArticle);

    String newCode(String idProvider);

    List<Article> listActive();

    Article getActive(String idArticle);

    void update(List<Article> articles) throws Exception;
}