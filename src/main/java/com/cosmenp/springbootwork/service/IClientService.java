package com.cosmenp.springbootwork.service;

import com.cosmenp.springbootwork.model.entity.Client;

import java.util.List;

public interface IClientService {
    void save(Client client) throws Exception;

    void update(Client client) throws Exception;

    List<Client> list();

    Client get(String idClient);

    String newCode(String postalCode);

    Client getByNif(String nif);
}