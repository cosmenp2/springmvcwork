package com.cosmenp.springbootwork.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

public interface IFileService {

    public Resource load(String filename) throws MalformedURLException;

    public void copy(MultipartFile file, String name) throws IOException;

    public boolean delete(String filename);

    public void deleteAll();

    public void init() throws IOException;
}
