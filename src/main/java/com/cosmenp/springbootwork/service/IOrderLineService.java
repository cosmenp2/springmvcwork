package com.cosmenp.springbootwork.service;

import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.entity.OrderLine;

import java.util.List;

public interface IOrderLineService {
    List<OrderLine> getLines(Order order);
}