package com.cosmenp.springbootwork.service;

import com.cosmenp.springbootwork.model.entity.Order;

import java.util.Date;
import java.util.List;

public interface IOrderService {
    void save(Order order);

    void update(Order order);

    List<Order> list();

    Order get(String idOrder);

    List<Order> listByClient(String idClient);

    List<Order> listByPaidState(boolean payed);

    List<Order> listAfter(Date date);

    List<Order> listBefore(Date date);

    List<Order> listBetween(Date before, Date after);

    List<Order> listByArticle(String idArticle);

    List<Order> listByProvider(String idProvider);

    String newCode(String postalCode);

    Order getByIdOrder(String idOrder);
}