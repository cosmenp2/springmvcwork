package com.cosmenp.springbootwork.service;

import com.cosmenp.springbootwork.model.entity.Provider;

import java.util.List;

public interface IProviderService {
    void save(Provider provider) throws Exception;

    List<Provider> list();

    Provider get(String idProvider);

    String newCode();

    void update(Provider provider) throws Exception;

    void disable(String idProvider);

    void enable(String idProvider);

    List<Provider> listActive();

    Provider getByCif(String cif);
}