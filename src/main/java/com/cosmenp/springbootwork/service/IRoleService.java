package com.cosmenp.springbootwork.service;

import com.cosmenp.springbootwork.model.entity.Role;

import java.util.List;

public interface IRoleService {
    Role get(int id);

    List<Role> list();

    Role findByRoleName(String role);
}