package com.cosmenp.springbootwork.service;

import com.cosmenp.springbootwork.model.entity.User;

import java.util.List;

public interface IUserService {
    void save(User user);

    void update(User user);

    List<User> list();

    User getByUsername(String username);

    User getByEmail(String email);

    User getByIdClient(String idClient);

    void disable(String username);

    void enable(String username);
}