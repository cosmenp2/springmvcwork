package com.cosmenp.springbootwork.service.impl;

import com.cosmenp.springbootwork.model.entity.Article;
import com.cosmenp.springbootwork.model.repository.ArticleRepository;
import com.cosmenp.springbootwork.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ArticleService implements IArticleService {
    @Autowired
    private ArticleRepository articleRepository;

    @Override
    @Transactional
    public void save(Article article) throws Exception {
        articleRepository.save(article);
    }

    @Override
    @Transactional
    public void update(Article article) throws Exception {
        Article oldArticle = articleRepository.findById(article.getId()).orElse(null);
        if (oldArticle != null) {
            oldArticle.setProvider(article.getProvider());
            oldArticle.setName(article.getName());
            oldArticle.setDescription(article.getDescription());
            oldArticle.setManufacture(article.getManufacture());
            oldArticle.setCost(article.getCost());
            oldArticle.setPvp(article.getPvp());
            oldArticle.setIva(article.getIva());
            oldArticle.setStock(article.getStock());
            oldArticle.setPhoto(article.getPhoto());
            oldArticle.setActive(article.getActive());
            articleRepository.save(oldArticle);
        } else {
            save(article);
        }
    }

    @Override
    public List<Article> list() {
        return (List<Article>) articleRepository.findAll();
    }

    @Override
    public Article get(String idArticle) {
        return articleRepository.findByIdArticle(idArticle);
    }

    @Override
    public void disable(String idArticle) {
        Article oldArticle = articleRepository.findByIdArticle(idArticle);
        if (oldArticle != null) {
            oldArticle.setActive(0);
            articleRepository.save(oldArticle);
        }
    }

    @Override
    public void enable(String idArticle) {
        Article oldArticle = articleRepository.findByIdArticle(idArticle);
        if (oldArticle != null) {
            oldArticle.setActive(1);
            articleRepository.save(oldArticle);
        }
    }

    @Override
    public String newCode(String idProvider) {
        String maxCode = articleRepository.getMaxCode(idProvider);
        String newCode = idProvider;
        if (maxCode != null) {
            int count = Integer.parseInt(maxCode.substring(4));
            count++;
            if (count < 10) {
                newCode += "00" + count;
            } else if ((count < 100)) {
                newCode += "0" + count;
            } else {
                newCode += Integer.toString(count);
            }
        } else {
            newCode += "001";
        }
        return newCode;
    }

    @Override
    public List<Article> listActive() {
        return articleRepository.findActive();
    }

    @Override
    public Article getActive(String idArticle) {
        return articleRepository.findByIdArticleActive(idArticle);
    }

    @Override
    @Transactional
    public void update(List<Article> articles) throws Exception {
        for (Article article : articles) {
            update(article);
        }
    }
}
