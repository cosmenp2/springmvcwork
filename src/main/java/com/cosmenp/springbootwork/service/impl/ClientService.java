package com.cosmenp.springbootwork.service.impl;

import com.cosmenp.springbootwork.model.entity.Client;
import com.cosmenp.springbootwork.model.repository.ClientRepository;
import com.cosmenp.springbootwork.service.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientService implements IClientService {
    @Autowired
    private ClientRepository clientRepository;

    @Override
    @Transactional
    public void save(Client client) throws Exception {
        clientRepository.save(client);
    }

    @Override
    @Transactional
    public void update(Client client) throws Exception {
        Client oldClient = clientRepository.findById(client.getId()).orElse(null);
        if (oldClient != null) {
            oldClient.setName(client.getName());
            oldClient.setSurname1(client.getSurname1());
            oldClient.setSurname2(client.getSurname2());
            oldClient.setNif(client.getNif());
            oldClient.setBusinessName(client.getBusinessName());
            oldClient.setPostalCode(client.getPostalCode());
            oldClient.setAddress(client.getAddress());
            oldClient.setPhoto(client.getPhoto());
            clientRepository.save(oldClient);
        } else {
            save(client);
        }
    }

    @Override
    public List<Client> list() {
        return (List<Client>) clientRepository.findAll();
    }

    @Override
    public Client get(String idClient) {
        return clientRepository.findByIdClient(idClient);
    }

    @Override
    public String newCode(String postalCode) {
        String maxCode = clientRepository.getMaxCode(postalCode);
        String newCode = postalCode;
        if (maxCode != null) {
            int count = Integer.parseInt(maxCode.substring(5));
            count++;
            if (count < 10) {
                newCode += "00" + count;
            } else if ((count < 100)) {
                newCode += "0" + count;
            } else {
                newCode += Integer.toString(count);
            }
        } else {
            newCode += "001";
        }
        return newCode;
    }

    @Override
    public Client getByNif(String nif) {
        return clientRepository.findByNif(nif);
    }
}
