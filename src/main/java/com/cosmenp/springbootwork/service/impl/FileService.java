package com.cosmenp.springbootwork.service.impl;

import com.cosmenp.springbootwork.service.IFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileService implements IFileService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private static String UPLOADS_FOLDER;

    @Override
    public Resource load(String filename) throws MalformedURLException {
        Path pathFoto = getPath(filename);
        log.info("pathFoto: " + pathFoto);

        Resource recurso = new UrlResource(pathFoto.toUri());

        if (!recurso.exists() || !recurso.isReadable()) {
            throw new RuntimeException("Error: no se puede cargar la imagen: " + pathFoto.toString());
        }
        return recurso;
    }

    @Override
    public void copy(MultipartFile file, String name) throws IOException {
        Path rootPath = getPath(name);

        log.info("rootPath: " + rootPath);

        Files.copy(file.getInputStream(), rootPath, StandardCopyOption.REPLACE_EXISTING);
    }

    @Override
    public boolean delete(String filename) {
        Path rootPath = getPath(filename);
        File archivo = rootPath.toFile();

        if (archivo.exists() && archivo.canRead()) {
            if (archivo.delete()) {
                return true;
            }
        }
        return false;
    }

    public Path getPath(String filename) {
        return Paths.get(UPLOADS_FOLDER).resolve(filename).toAbsolutePath();
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(Paths.get(UPLOADS_FOLDER).toFile());

    }

    @Override
    public void init() throws IOException {
        UPLOADS_FOLDER = System.getProperty("user.home") + "/uploads_nieto_srping";
        Files.createDirectory(Paths.get(UPLOADS_FOLDER));
    }
}
