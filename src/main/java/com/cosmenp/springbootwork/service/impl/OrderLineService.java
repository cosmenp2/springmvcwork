package com.cosmenp.springbootwork.service.impl;

import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.entity.OrderLine;
import com.cosmenp.springbootwork.model.repository.OrderLineRepository;
import com.cosmenp.springbootwork.service.IOrderLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderLineService implements IOrderLineService {
    @Autowired
    private OrderLineRepository orderLineRepository;


    @Override
    public List<OrderLine> getLines(Order order) {
        return orderLineRepository.findByOrder(order);
    }
}
