package com.cosmenp.springbootwork.service.impl;

import com.cosmenp.springbootwork.model.entity.Client;
import com.cosmenp.springbootwork.model.entity.Order;
import com.cosmenp.springbootwork.model.repository.OrderRepository;
import com.cosmenp.springbootwork.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class OrderService implements IOrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Override
    @Transactional
    public void save(Order order) {
        orderRepository.save(order);
    }

    @Override
    @Transactional
    public void update(Order order) {
        Order oldOrder = orderRepository.findById(order.getId()).orElse(null);
        if (oldOrder != null) {
            oldOrder.setPaid(order.getPaid());
            oldOrder.setActive(order.getActive());
            orderRepository.save(oldOrder);
        } else {
            save(order);
        }
    }

    @Override
    public List<Order> list() {
        return (List<Order>) orderRepository.findAll();
    }

    @Override
    public Order get(String idOrder) {
        return orderRepository.findByIdOrder(idOrder);
    }

    @Override
    public List<Order> listByClient(String idClient) {
        Client filter = new Client();
        filter.setIdClient(idClient);
        return orderRepository.findByClient(filter);
    }

    @Override
    public List<Order> listByPaidState(boolean payed) {
        if (payed) {
            return orderRepository.findByPaid(1);
        } else {
            return orderRepository.findByPaid(0);
        }
    }

    @Override
    public List<Order> listAfter(Date date) {
        return orderRepository.findByDateAfter(date);
    }

    @Override
    public List<Order> listBefore(Date date) {
        return orderRepository.findByDateBefore(date);
    }

    @Override
    public List<Order> listBetween(Date before, Date after) {
        return orderRepository.findByDateBetween(before, after);
    }

    @Override
    public List<Order> listByArticle(String idArticle) {
        List<Order> orders = orderRepository.findOrdersByIdArticle(idArticle);
        return orders;
    }

    @Override
    public List<Order> listByProvider(String idProvider) {
        return orderRepository.findByIdProvider(idProvider);
    }

    @Override
    public String newCode(String postalCode) {
        String maxCode = orderRepository.getMaxCode(postalCode);
        String newCode = postalCode;
        if (maxCode != null) {
            int count = Integer.parseInt(maxCode.substring(5));
            count++;
            if (count < 10) {
                newCode += "000" + count;
            } else if ((count < 100)) {
                newCode += "00" + count;
            } else if ((count < 1000)) {
                newCode += "0" + count;
            } else {
                newCode += Integer.toString(count);
            }
        } else {
            newCode += "0001";
        }
        return newCode;
    }

    @Override
    public Order getByIdOrder(String idOrder) {
        return orderRepository.findByIdOrder(idOrder);
    }
}
