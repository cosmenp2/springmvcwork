package com.cosmenp.springbootwork.service.impl;

import com.cosmenp.springbootwork.model.entity.Article;
import com.cosmenp.springbootwork.model.entity.Provider;
import com.cosmenp.springbootwork.model.repository.ProviderRepository;
import com.cosmenp.springbootwork.service.IProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProviderService implements IProviderService {
    @Autowired
    private ProviderRepository providerRepository;

    @Override
    @Transactional
    public void save(Provider provider) throws Exception {
        providerRepository.save(provider);
    }

    @Override
    public List<Provider> list() {
        return (List<Provider>) providerRepository.findAll();
    }

    @Override
    public Provider get(String idProvider) {
        return providerRepository.findByIdProvider(idProvider);
    }

    @Override
    public String newCode() {
        String maxCode = providerRepository.getMaxCode();
        String newCode;
        if (maxCode != null) {
            int count = Integer.parseInt(maxCode);
            count++;
            if (count < 10) {
                newCode = "000" + count;
            } else if ((count < 100)) {
                newCode = "00" + count;
            } else if (count < 1000) {
                newCode = "0" + count;
            } else {
                newCode = Integer.toString(count);
            }
        } else {
            newCode = "0001";
        }
        return newCode;
    }

    @Override
    @Transactional
    public void update(Provider provider) throws Exception {
        Provider oldProvider = providerRepository.findById(provider.getId()).orElse(null);
        if (oldProvider != null) {
            oldProvider.setName(provider.getName());
            oldProvider.setBusinessName(provider.getBusinessName());
            oldProvider.setCif(provider.getCif());
            oldProvider.setPhoto(provider.getPhoto());
            oldProvider.setActive(provider.getActive());
            providerRepository.save(oldProvider);
        } else {
            save(provider);
        }
    }

    @Override
    public void disable(String idProvider) {
        Provider oldProvider = providerRepository.findByIdProvider(idProvider);
        if (oldProvider != null) {
            for (Article article : oldProvider.getArticles()) {
                article.setActive(0);
            }
            oldProvider.setActive(0);
            providerRepository.save(oldProvider);
        }
    }

    @Override
    public void enable(String idProvider) {
        Provider oldProvider = providerRepository.findByIdProvider(idProvider);
        if (oldProvider != null) {
            oldProvider.setActive(1);
            providerRepository.save(oldProvider);
        }
    }

    @Override
    public List<Provider> listActive() {
        return providerRepository.findByActiveEquals(1);
    }

    @Override
    public Provider getByCif(String cif) {
        return providerRepository.findByCif(cif);
    }
}
