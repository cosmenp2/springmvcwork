package com.cosmenp.springbootwork.service.impl;

import com.cosmenp.springbootwork.model.entity.Role;
import com.cosmenp.springbootwork.model.repository.RoleRepository;
import com.cosmenp.springbootwork.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService implements IRoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role get(int id) {
        return roleRepository.findById(id).orElse(null);
    }

    @Override
    public List<Role> list() {
        return (List<Role>) roleRepository.findAll();
    }

    @Override
    public Role findByRoleName(String role) {
        return roleRepository.findByRole(role);
    }
}
