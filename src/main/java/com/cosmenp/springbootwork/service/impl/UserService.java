package com.cosmenp.springbootwork.service.impl;

import com.cosmenp.springbootwork.model.entity.User;
import com.cosmenp.springbootwork.model.repository.UserRepository;
import com.cosmenp.springbootwork.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void update(User user) {
        User oldUser = userRepository.findById(user.getId()).orElse(null);
        if (oldUser != null) {
            oldUser.setUsername(user.getUsername());
            oldUser.setPassword(user.getPassword());
            oldUser.setEmail(user.getEmail());
            oldUser.setRole(user.getRole());
            userRepository.save(oldUser);
        } else {
            save(user);
        }
    }

    @Override
    public List<User> list() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User getByIdClient(String idClient) {
        return userRepository.findByClient_IdClient(idClient);
    }

    @Override
    public void disable(String username) {
        User oldUser = userRepository.findByUsername(username);
        if (oldUser != null) {
            oldUser.setActive(0);
            userRepository.save(oldUser);
        }
    }

    @Override
    public void enable(String username) {
        User oldUser = userRepository.findByUsername(username);
        if (oldUser != null) {
            oldUser.setActive(1);
            userRepository.save(oldUser);
        }
    }
}
