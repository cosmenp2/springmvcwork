insert into spring_roles (role) values ('ADMIN');
insert into spring_roles (role) values ('USER');
insert into spring_users (username, password, email, id_client, id_role,active) values ('admin', '$2a$10$RXfmrktaYzVCCD.4OQjd6uU9l3iIuC8Vmn2uMgas7abhVv7e.4uT6', 'admin@example.com', null, 1,1);
